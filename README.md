# README #

Getting started with Asphalt.js

### What is Asphalt.js? ###

* A very simple framework-agnostic JavaScript data binding library.
* 1.0a1 (first alpha release)

### How do I get set up? ###

1. Include Asphalt in your project, like this: ```<script src="asphalt.js"></script>```
2. Create a data model to which to bind your UI, like this: ```<script>var model = { foo: "bar" };</script>```
3. Add bindings to your UI, like this: ```<span data-bind="foo"></span>```
4. Bind it all together with Asphalt, like this: ```<script>var asphalt = new Asphalt(); asphalt.Bind(model);</script>```
5. You're done! Check out the [wiki](https://bitbucket.org/benfarhner/asphalt) for more information

### Who do I talk to? ###

* [benfarhner](https://bitbucket.org/benfarhner)
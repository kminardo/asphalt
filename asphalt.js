/*

Asphalt.js

A very simple framework-agnostic data binder.

(c) 2015 Ben Farhner, Farhner Manufacturing Company

*/
/*jslint white: true, plusplus: true */
/*global console */

// new Asphalt()
// Creates a new instance of Asphalt.
// enableLogging: (optional) Whether or not to send log messages to the console. Defaults to false
var Asphalt = function (enableLogging) {
    'use strict';
    
    this.enableLogging = (enableLogging === true);
    this.Log('Warming up bitumen...');
};

Asphalt.prototype = {
    /*
     * Properties
     */
    
    Formatters: {},
    
    /*
     * Methods
     */
    
    // Constructor
    // new Asphalt()
    constructor: Asphalt,
    
    // Log()
    // Wrapper for console logging.
    Log: function (message) {
        'use strict';
        
        if (typeof console !== 'undefined' && this.enableLogging) {
            console.log('[Asphalt] ' + message);
        }
    },

    // GetArrayItemId()
    // Builds a unique ID for an array item given the binding path and array index.
    // Returns: string
    GetArrayItemId: function (bindingPath, index) {
        'use strict';
        
        return bindingPath.replace(/\./g, '-') + '-' + index;
    },

    // IsInputElement()
    // Determines whether or not the given element is a form input element.
    // Returns: bool
    IsInputElement: function (element) {
        'use strict';
        
        if (typeof element !== 'undefined' && element !== null &&
            (element.tagName === 'INPUT' || element.tagName === 'SELECT')) {
            return true;
        }
        
        return false;
    },
    
    // IsBoolInputElement()
    // Determines whether or not the given element is a boolean-based form input element, i.e. a radio button or checkbox.
    // Returns: bool
    IsBoolInputElement: function (element) {
        'use strict';
        
        if (typeof element !== 'undefined' && element !== null && element.tagName === 'INPUT' &&
            (element.type === 'radio' || element.type === 'checkbox')) {
            return true;
        }
        
        return false;
    },

    // GetPropertyFromPath()
    // Gets the property name from the given binding path.
    // Returns: string
    GetPropertyFromPath: function (bindingPath) {
        'use strict';
        
        var pathParts;
        
        bindingPath = bindingPath || '';
        pathParts = bindingPath.split('.');
        
        return pathParts[pathParts.length - 1];
    },

    // GetBindingObject()
    // Finds the object in the given model containing the bound property in the given binding path.
    // Returns: object
    GetBindingObject: function (model, bindingPath) {
        'use strict';
        
        var pathParts,
            bindingObject,
            index;
        
        if (typeof model === 'undefined' || model === null) {
            return null;
        }
        
        bindingPath = bindingPath || '';
        
        // Split the path by dot notation
        pathParts = bindingPath.split('.');
        
        // Initially point to the model itself
        bindingObject = model;
        
        // Loop over property parts to find the property in the model
        for (index = 0; index < pathParts.length; index++) {
            if (bindingObject.hasOwnProperty(pathParts[index])) {
                // Only travel down to the parent object, not the actual binding value
                if (index < pathParts.length - 1) {
                    bindingObject = bindingObject[pathParts[index]];
                }
            }
            else {
                return null;
            }
        }
        
        return bindingObject;
    },
    
    // GetFormattedValue()
    // Returns the given value, formatted by the given element's formatter if available
    GetFormattedValue: function (element, value) {
        'use strict';
        
        var formatterName,
            formatter;

        if (typeof element === 'undefined' || element === null ||
            typeof value === 'undefined' || value === null) {
            return null;
        }
        
        // Check for formatter
        if (element.hasAttribute('data-bind-formatter')) {
            formatterName = element.getAttribute('data-bind-formatter');

            if (this.Formatters.hasOwnProperty(formatterName)) {
                value = this.Formatters[formatterName](value);
            }
            else {
                this.Log('Invalid data-bind-formatter: "' + formatterName + '"');
            }
        }
        
        return value;
    },

    // SetElementValue()
    // Sets either the text value or field value of the given DOM element.
    SetElementValue: function (element, value) {
        'use strict';
        
        var type;

        if (typeof element === 'undefined' || element === null ||
            typeof value === 'undefined' || value === null) {
            return;
        }
        
        if (this.IsBoolInputElement(element)) {
            // Special case for checkboxes and radio buttons
            if (value) {
                element.checked = true;
            }
            else {
                element.checked = false;
            }
        }
        else if (this.IsInputElement(element)) {
            element.value = value;
        }
        else {
            // Check for binding type
            type = element.getAttribute('data-bind-type') || 'text';
            
            if (type === 'text') {                
                element.textContent = this.GetFormattedValue(element, value);
            }
            else if (type === 'html') {
                element.innerHTML = value;
            }
            else {
                this.Log('Invalid data-bind-type: "' + type + '"');
            }
        }
    },

    // SetSelectOptions()
    // For <select> inputs, checks for provided source and binds that to the dropdown options.
    SetSelectOptions: function (element, model) {
        'use strict';
        
        var that = this,
            bindingPath,
            bindingObject,
            bindingProperty,
            bindingArray,
            optionElement,
            index;

        if (typeof element === 'undefined' || element === null ||
            typeof model === 'undefined' || model === null) {
            return;
        }

        if (element.tagName === 'SELECT' && element.hasAttribute('data-bind-options')) {
            bindingPath = element.getAttribute('data-bind-options') || '';
            bindingObject = this.GetBindingObject(model, bindingPath);

            if (bindingObject === null) {
                this.Log('Could not bind options to "' + bindingPath + '": it does not exist in the model!');

                return false;
            }

            bindingProperty = this.GetPropertyFromPath(bindingPath);
            bindingArray = bindingObject[bindingProperty];

            // Remove any existing elements
            while (element.hasChildNodes()) {
                element.removeChild(element.firstChild);
            }

            // Loop over all the items in the array
            for (index = 0; index < bindingArray.length; index++) {
                optionElement = document.createElement('option');

                // Check for Value property
                if (bindingArray[index].hasOwnProperty('Value')) {
                    optionElement.value = bindingArray[index].Value;
                }
                else if (bindingArray[index].hasOwnProperty('value')) {
                    optionElement.value = bindingArray[index].value;
                }
                else {
                    optionElement.value = bindingArray[index];
                }
                
                // Check for formatter
                if (element.hasAttribute('data-bind-formatter')) {
                    optionElement.textContent = this.GetFormattedValue(element, bindingArray[index]);
                }
                else {
                    optionElement.textContent = bindingArray[index];
                }

                element.appendChild(optionElement);
            }
        }
    },

    // BindElement()
    // Binds the given DOM element using the given model
    // element: DOM element to which to bind
    // model: Model to bind to the DOM element
    BindElement: function (element, model) {
        'use strict';
        
        var that = this,
            bindingPath,
            bindingObject,
            bindingProperty;

        if (typeof element === 'undefined' || element === null ||
            typeof model === 'undefined' || model === null) {
            return false;
        }

        bindingPath = element.getAttribute('data-bind') || '';
        bindingObject = this.GetBindingObject(model, bindingPath);

        if (bindingObject === null) {
            this.Log('Could not bind to "' + bindingPath + '": it does not exist in the model!');

            return false;
        }

        bindingProperty = this.GetPropertyFromPath(bindingPath);

        // Set select source if needed
        this.SetSelectOptions(element, model);

        // Set initial value on the element
        this.SetElementValue(element, bindingObject[bindingProperty]);

        if (this.IsBoolInputElement(element)) {
            // Listen for changes to the input
            element.addEventListener('change', function (event) {
                bindingObject[bindingProperty] = this.checked;
            });
            element.addEventListener('keyup', function (event) {
                bindingObject[bindingProperty] = this.checked;
            });
        }
        else if (this.IsInputElement(element)) {
            // Listen for changes to the input
            element.addEventListener('change', function (event) {
                bindingObject[bindingProperty] = this.value;
            });
            element.addEventListener('keyup', function (event) {
                bindingObject[bindingProperty] = this.value;
            });
        }

        // Observe future changes to the value
        Object.observe(bindingObject, function (changes) {
            changes.forEach(function (change) {
                if (change.name === bindingProperty) {
                    that.SetElementValue(element, change.object[change.name]);
                }
            });
        });
    },

    // BindArrayItemElement()
    // Binds a specific array item to the given model using the given template
    BindArrayItemElement: function (template, model) {
        'use strict';
        
        var arrayItemElement,
            boundElements,
            index;

        if (typeof template === 'undefined' || template === null ||
            typeof model === 'undefined' || model === null) {
            // Nothing to bind!
            return null;
        }

        // Clone the template
        arrayItemElement = document.importNode(template.content, true);

        // Bind elements in the template
        boundElements = arrayItemElement.querySelectorAll('[data-bind]');

        // Loop over each bound element and find the property in the model
        for (index = 0; index < boundElements.length; index++) {
            this.BindElement(boundElements[index], model);
        }

        // Return the newly templated item
        return arrayItemElement;
    },

    // BindArrayElement()
    // Binds given array element to the given model
    BindArrayElement: function (element, model) {
        'use strict';
        
        var that = this,
            bindingPath,
            bindingObject,
            bindingProperty,
            bindingArray,
            index,
            template,
            arrayItemElement;

        if (typeof element === 'undefined' || element === null ||
            typeof model === 'undefined' || model === null) {
            // Nothing to bind!
            return;
        }

        bindingPath = element.getAttribute('data-bind-array') || '';
        bindingObject = this.GetBindingObject(model, bindingPath);

        if (bindingObject === null) {
            this.Log('Could not bind to "' + bindingPath + '": it does not exist in the model!');

            return;
        }

        bindingProperty = this.GetPropertyFromPath(bindingPath);
        bindingArray = bindingObject[bindingProperty];

        // Get the template
        template = element.querySelector('template');

        if (template === null) {
            this.Log('Could not bind to "' + bindingPath + '": no template available!');

            return;
        }

        // Loop over all the items in the array
        for (index = 0; index < bindingArray.length; index++) {
            arrayItemElement = this.BindArrayItemElement(template, bindingArray[index]);

            // Set template GUID
            if (arrayItemElement.firstElementChild !== null) {
                arrayItemElement.firstElementChild.id = this.GetArrayItemId(element.getAttribute('data-bind-array'), index);
            }

            element.appendChild(arrayItemElement);
        }

        // Observe future changes to the array
        Array.observe(bindingArray, function (changes) {
            changes.forEach(function (change) {
                var index,
                    arrayItemElement,
                    insertBeforeElement = null;

                if (change.type === 'add') {
                    // Template out new item and append at the end
                    arrayItemElement = that.BindArrayItemElement(template, change.object[change.object.length - 1]);

                    // Set template GUID
                    if (arrayItemElement.firstElementChild !== null) {
                        arrayItemElement.firstElementChild.id = that.GetArrayItemId(element.getAttribute('data-bind-array'), change.index + index);
                    }

                    // Insert templated item at the correct location
                    element.appendChild(arrayItemElement);
                }
                else if (change.type === 'splice') {
                    // Remove all removed items
                    for (index = 0; index < change.removed.length; index++) {
                        arrayItemElement = document.getElementById(that.GetArrayItemId(element.getAttribute('data-bind-array'), change.index + index));
                        arrayItemElement.remove();
                    }

                    // Get element at the splice index after which to insert new items
                    if (change.index > 0) {
                        insertBeforeElement = document.getElementById(that.GetArrayItemId(element.getAttribute('data-bind-array'), change.index - 1)).nextElementSibling;
                    }

                    // Add all added items
                    for (index = 0; index < change.addedCount; index++) {
                        arrayItemElement = that.BindArrayItemElement(template, change.object[change.index + index]);

                        // Set template GUID
                        if (arrayItemElement.firstElementChild !== null) {
                            arrayItemElement.firstElementChild.id = that.GetArrayItemId(element.getAttribute('data-bind-array'), change.index + index);
                        }

                        // Insert templated item at the correct location
                        element.insertBefore(arrayItemElement, insertBeforeElement);
                    }
                }
            });
        });
    },
    
    // Bind()
    // Binds the given model to the UI.
    Bind: function (model) {
        'use strict';
        
        var boundElements,
            index;

        if (typeof model === 'undefined' || model === null) {
            return;
        }

        this.Log('Pouring pavement...');

        // Grab all bound elements
        boundElements = document.querySelectorAll('[data-bind]');

        // Loop over each bound element and bind it
        for (index = 0; index < boundElements.length; index++) {
            this.BindElement(boundElements[index], model);
        }

        // Grab all bound array elements
        boundElements = document.querySelectorAll('[data-bind-array]');

        // Loop over each bound array element and bind it
        for (index = 0; index < boundElements.length; index++) {
            this.BindArrayElement(boundElements[index], model);
        }

        this.Log('Pavement has cooled.');
    },
    
    // AddFormatter()
    // Adds a new formatter that can be used with data-bind-formatter.
    AddFormatter: function (name, formatter) {
        'use strict';
        
        if (typeof name !== 'string' || name === null || name.trim() === '' ||
            typeof formatter !== 'function' || formatter === null) {
            return false;
        }
        
        // Add formatter to list of available formatters
        this.Formatters[name] = formatter;
        
        this.Log('Added formatter "' + name + '"');
        
        return true;
    }
};
